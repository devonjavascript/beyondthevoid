package org.voidbytes.util;

import org.lwjgl.Sys;

public class Time {
	private static Time instance = null;
	private static long lastUpdate;
	private static float resolution;
	
	public Time(){
		resolution = Sys.getTimerResolution();
		lastUpdate = getTime();
	}
	
	public static long getTime(){
		return (long) ((Sys.getTime() * 1000) / resolution);
	}
	
	public static long getDelta(){
		long delta = getTime() - lastUpdate;
		lastUpdate = getTime();
		return delta;
	}
	
	public static Time getInstance(){
		if(instance == null){
			instance = new Time();
		}
		return instance;
	}
}
