package org.voidbytes;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.voidbytes.audio.AudioSource;
import org.voidbytes.audio.AudioTest;
import org.voidbytes.audio.Listener;
import org.voidbytes.audio.Sound;
import org.voidbytes.util.Time;

public class Game {
	private static final int WIDTH = 720,HEIGHT = 403;
	private static Game instance = null;
	private static boolean running = false;
	
	public static void main(String[] args){
		getInstance();
	}
	
	public Game(){
		setupDisplay();
		loop();
	}
	
	public static Game getInstance(){
		if(instance == null){
			instance = new Game();
		}
		return instance;
	}
	
	private static void setupDisplay(){
		try{
			setDisplayMode(WIDTH,HEIGHT,false);
			Display.setResizable(true);
			Display.create();
			AL.create();
		}catch(LWJGLException e){
			e.printStackTrace();
			System.err.println();
		}
	}
	
	public static boolean setDisplayMode(int width,int height,boolean fullscreen){
		try {
			if(fullscreen){
			DisplayMode[] modes = Display.getAvailableDisplayModes();
			DisplayMode target = null;
			int freq = 0;
			//go through each availible mode
			for(DisplayMode mode: modes){
				if(mode.getWidth() == width && mode.getHeight() == height){
					if(mode.getBitsPerPixel() >= Display.getDesktopDisplayMode().getBitsPerPixel()){
						if(mode.getFrequency() >= freq){
							target = mode;
						}
					}
				}
			}
			Display.setDisplayMode(target);
			}else{
				Display.setDisplayMode(new DisplayMode(width,height));
			}
			return true;
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.err.println("Could not select a display mode.");
			return false;
		}		
	}
	
	private static void loop(){
		Time.getInstance();
		Sound sound = new Sound("res/wander.wav");
		AudioSource source = new AudioSource();
		source.setSound(sound);
		source.play();
		System.out.println(sound.getLength());
		Listener.getInstance();
		running = true;
		while(running && !Display.isCloseRequested()){
			long delta = Time.getDelta();
			if(Display.wasResized())
				resized();
			Listener.getInstance().update();
			source.update(delta);
			Display.sync(60);
			Display.update();
		}
		source.destroy();
		sound.destory();
		AL.destroy();
		end();
	}
	
	private static void resized(){
		System.out.println("Window resized to: "+Display.getWidth()+","+Display.getHeight());
	}
	
	public static void end(){
		running = false;
		Display.destroy();
		System.exit(0);
	}
}
