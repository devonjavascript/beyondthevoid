package org.voidbytes.audio;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.vector.Vector3f;

public class AudioSource {
	protected boolean fade = false,playing = false;
	public boolean loop = false;
	protected float clipLength,pitch = 1.0f,gain = 1.0f;
	private float timePlayed = 0;
	private IntBuffer source = BufferUtils.createIntBuffer(1);
	protected Sound sound;
	public Vector3f position,velocity;
	private FloatBuffer fPosition = BufferUtils.createFloatBuffer(3),fVelocity = BufferUtils.createFloatBuffer(3);
	
	public AudioSource(){
		AL10.alGenSources(source);
		position = new Vector3f(0,0,0);
		velocity = new Vector3f(0,0,0);
		update(0);
	}
	
	public void update(long elapsedTime){
		setPitch(pitch);
		setGain(gain);
		if(elapsedTime > 0){
			position.x += velocity.x * (1/elapsedTime);
			position.y += velocity.y * (1/elapsedTime);
			position.z += velocity.z * (1/elapsedTime);
		}
		fPosition  = (FloatBuffer) fPosition.put(new float[]{position.x,position.y,position.z}).rewind();
		fVelocity = (FloatBuffer) fVelocity.put(new float[]{velocity.x,velocity.y,velocity.z}).rewind();
		AL10.alSource(source.get(0), AL10.AL_POSITION, fPosition);
		AL10.alSource(source.get(0), AL10.AL_VELOCITY, fVelocity);
		if(AL10.alGetError() != AL10.AL_NO_ERROR){
			System.err.println("Error with updating");
			System.err.println(getALErrorString(AL10.alGetError()));
		}
		if(playing){
			timePlayed += (elapsedTime*0.001);
		}
		
		if(timePlayed >= clipLength && clipLength > 0){
			playing = false;
			System.err.println("Clip done playing");
			if(loop){
				stop();
				play();
				timePlayed = 0;
			}
		}

	}
	
	public static String getALErrorString(int err) {
		  switch (err) {
		    case AL10.AL_NO_ERROR:
		      return "AL_NO_ERROR";
		    case AL10.AL_INVALID_NAME:
		      return "AL_INVALID_NAME";
		    case AL10.AL_INVALID_ENUM:
		      return "AL_INVALID_ENUM";
		    case AL10.AL_INVALID_VALUE:
		      return "AL_INVALID_VALUE";
		    case AL10.AL_INVALID_OPERATION:
		      return "AL_INVALID_OPERATION";
		    case AL10.AL_OUT_OF_MEMORY:
		      return "AL_OUT_OF_MEMORY";
		    default:
		      return "No such error code";
		  }
		}
	
	public void setPitch(float pitch){
		this.pitch = pitch;
		AL10.alSourcef(source.get(0), AL10.AL_PITCH, pitch);
	}
	
	public void setGain(float gain){
		this.gain = gain;
		AL10.alSourcef(source.get(0), AL10.AL_GAIN, gain);
	}
	
	public void setSound(Sound sound){
		AL10.alSourceStop(source.get(0));
		AL10.alSourcei(source.get(0),AL10.AL_BUFFER,sound.getBuffer().get(0));
		timePlayed = 0;
		playing = false;
		this.sound = sound;
		update(0);
	}
	
	public void play(){
		AL10.alSourcePlay(source.get(0));
		clipLength = sound.getLength();
		playing = true;
	}
	
	public void stop(){
		AL10.alSourceStop(source.get(0));
		playing = false;
		timePlayed = 0;
	}
	
	public void pause(){
		AL10.alSourcePause(source.get(0));
		playing = false;
	}
	
	public void setFading(boolean fading){
		fade = fading;
	}
	
	public Sound getSound(){
		return sound;
	}
	
	public void destroy(){
		AL10.alDeleteSources(source);
	}
}
