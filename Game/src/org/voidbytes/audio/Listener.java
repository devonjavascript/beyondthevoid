package org.voidbytes.audio;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.vector.Vector3f;

public class Listener {
	protected static Listener instance = null;
	public Vector3f position = new Vector3f(0f,0f,0f);
	public Vector3f velocity = new Vector3f(0f,0f,0f);
	public Vector3f rotation = new Vector3f(0f,0f,-1f);
	
	private FloatBuffer listenerPos = BufferUtils.createFloatBuffer(3);
	private FloatBuffer listenerVel = BufferUtils.createFloatBuffer(3);
	private FloatBuffer listenerOri = BufferUtils.createFloatBuffer(6);
	
	public static Listener getInstance(){
		if(instance == null){
			instance = new Listener();
		}
		return instance;
	}
	
	public Listener(){
		listenerPos = (FloatBuffer) listenerPos.put(new float[]{position.x,position.y,position.z}).rewind();
		listenerVel = (FloatBuffer) listenerVel.put(new float[]{velocity.x,velocity.y,velocity.z}).rewind();
		listenerOri = (FloatBuffer) listenerOri.put(new float[]{rotation.x,rotation.y,rotation.x,0f,1f,0f}).rewind();
		update();
	}
	
	public void update(){
		AL10.alListener(AL10.AL_POSITION, listenerPos);
		AL10.alListener(AL10.AL_ORIENTATION,listenerOri);
		AL10.alListener(AL10.AL_VELOCITY, listenerVel);
	}
}
