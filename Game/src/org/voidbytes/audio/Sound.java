package org.voidbytes.audio;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

public class Sound {
	protected int id;
	protected String path;
	private IntBuffer buffer;
	
	public Sound(String path){
		this.path = path;
		buffer = BufferUtils.createIntBuffer(1);
		AL10.alGenBuffers(buffer);
		try{
			FileInputStream fin = new FileInputStream(path);
			BufferedInputStream bin = new BufferedInputStream(fin);
			WaveData data = WaveData.create(bin);
			if(data != null){
				AL10.alBufferData(buffer.get(0), data.format, data.data, data.samplerate);
				data.dispose();
			}
			bin.close();
			fin.close();
			id = buffer.get(0);
		}catch(FileNotFoundException e){
			e.printStackTrace();
			System.err.println("Could not find file "+path);
		}catch(IOException e){
			e.printStackTrace();
			System.err.println("Error when reading file "+path);
		}
	}
	
	public IntBuffer getBuffer(){
		return buffer;
	}
	
	public float getLength(){
		int size,bits,channels,freq;
		size = AL10.alGetBufferi(buffer.get(0), AL10.AL_SIZE);
		bits = AL10.alGetBufferi(buffer.get(0), AL10.AL_BITS);
		channels = AL10.alGetBufferi(buffer.get(0), AL10.AL_CHANNELS);
		freq = AL10.alGetBufferi(buffer.get(0), AL10.AL_FREQUENCY);
		return (float) ((int)size/channels/(bits/8)) / (float)freq;
	}
	
	
	public void destory(){
		AL10.alDeleteBuffers(buffer);
	}
}
