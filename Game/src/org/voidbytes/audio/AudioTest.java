package org.voidbytes.audio;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

public class AudioTest {
	IntBuffer buffer = BufferUtils.createIntBuffer(1);
	IntBuffer source = BufferUtils.createIntBuffer(1);
	FloatBuffer sourcePos = (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[]{0f,0f,0f}).rewind();
	FloatBuffer sourceVel = (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[]{0f,0f,0f}).rewind();
	FloatBuffer listenerOri = (FloatBuffer) BufferUtils.createFloatBuffer(6).put(new float[]{0f,0f,-1f,0f,1f,0f}).rewind();
	FloatBuffer listenerPos = (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[]{0f,0f,0f}).rewind();
	FloatBuffer listenerVel = (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[]{0f,0f,0f}).rewind();
	String path;
	
	public AudioTest(String path){
		this.path = path;
		System.out.println("path: "+path);
		try{
			AL.create();
		}catch(LWJGLException e){
			e.printStackTrace();
		}
		AL10.alGetError();
		
		if(loadALData() == AL10.AL_FALSE){
			System.err.println("Error laoding data");
		}
		
		setListenerValues();
		
		AL10.alSourcePlay(source.get(0));
	}
	int loadALData(){
		AL10.alGenBuffers(buffer);
		if(AL10.alGetError() != AL10.AL_NO_ERROR){
			return AL10.AL_FALSE;
		}
		
		try{
			FileInputStream fin = new FileInputStream(path);
			BufferedInputStream bin = new BufferedInputStream(fin);
			WaveData data = WaveData.create(bin);
			AL10.alBufferData(buffer.get(0), data.format, data.data, data.samplerate);
			data.dispose();
			bin.close();
			fin.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
			System.err.println("Could not find file.");
		}catch(IOException e){
			e.printStackTrace();
		}
		
		AL10.alGenSources(source);
		
		if(AL10.alGetError() != AL10.AL_NO_ERROR){
			return AL10.AL_FALSE;
		}
		AL10.alSourcei(source.get(0), AL10.AL_BUFFER, buffer.get(0));
		AL10.alSourcef(source.get(0), AL10.AL_PITCH, 1.0f);
		AL10.alSourcef(source.get(0), AL10.AL_GAIN, 1.0f);
		AL10.alSource(source.get(0), AL10.AL_POSITION, sourcePos);
		AL10.alSource(source.get(0), AL10.AL_VELOCITY, sourceVel);
		
		if(AL10.alGetError() == AL10.AL_NO_ERROR){
			return AL10.AL_TRUE;
		}
		
		return AL10.AL_FALSE;
	}
	
	public void setListenerValues(){
		AL10.alListener(AL10.AL_POSITION,listenerPos);
		AL10.alListener(AL10.AL_VELOCITY, listenerVel);
		AL10.alListener(AL10.AL_ORIENTATION, listenerOri);
	}
	
	public void killALData(){
		AL10.alDeleteSources(source);
		AL10.alDeleteBuffers(buffer);
	}
	
	public void play(){
		 AL10.alSourcePlay(source.get(0));
	}
	
	public void pause(){
		AL10.alSourcePause(source.get(0));
	}
	
	public void stop(){
		AL10.alSourceStop(source.get(0));
	}
	
	public void close(){
		killALData();
		AL.destroy();
	}
}
